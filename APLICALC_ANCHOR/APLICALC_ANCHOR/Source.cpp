#include<iostream>
#include <math.h>
using namespace std;

//Origin at lower-left
class Rect
{
public:
	float xPos;
	float yPos;
	float width;
	float height;
	float anchorX;
	float anchorY;
	Rect(float p_x, float p_y, float p_width, float p_height, float anchorX, float p_anchorY);

	bool didIntersectRect(Rect rectB);
};

Rect::Rect(float p_x, float p_y, float p_width, float p_height, float p_anchorX, float p_anchorY)
{
	xPos = p_x;
	yPos = p_y;
	width = p_width;
	height = p_height;
	anchorX = p_anchorX;
	anchorY = p_anchorY;
}

bool Rect::didIntersectRect(Rect rectB)
{
	bool l_result = false;

	bool l_xIntersect = false;
	bool l_yIntersect = false;

	float rect1Left = xPos - (width * anchorX);
	float rect1Right = xPos + (width *(1.0f- anchorX));
	float rect1Down = yPos - (height * anchorY);
	float rect1Up = yPos + (height *(1.0f - anchorY));

	float rect2Left = rectB.xPos - (rectB.width * rectB.anchorX);
	float rect2Right = rectB.xPos + (rectB.width *(1.0f - rectB.anchorX));
	float rect2Down = rectB.yPos - (rectB.height * rectB.anchorY);
	float rect2Up = rectB.yPos + (rectB.height *(1.0f - rectB.anchorY));

	//check if intersects x-axis
	if (rect1Left >= rect2Left && rect1Left <= rect2Right)
	{
		l_xIntersect = true;
	}
	else if (rect1Right >= rect2Left && rect1Right <= rect2Right)
	{
		l_xIntersect = true;
	}

	//check if intersects y-axis
	if (rect1Down >= rect2Down && rect1Down <= rect2Up)
	{
		l_yIntersect = true;
	}
	else if (rect1Up >= rect2Down && rect1Up <= rect2Up)
	{
		l_yIntersect = true;
	}

	l_result = l_xIntersect && l_yIntersect;

	return l_result;
}

class Circ
{
public:
	float xPos;
	float yPos;
	float radius;
	Circ(float p_x, float p_y, float p_radius);

	bool didIntersectCirc(Circ circB);
};

Circ::Circ(float p_x, float p_y, float p_radius)
{
	xPos = p_x;
	yPos = p_y;
	radius = p_radius;
}

bool Circ::didIntersectCirc(Circ circB)
{
	bool l_result = false;
	float x = xPos - circB.xPos; 
	float y = yPos - circB.yPos;
	float dist = pow(x, 2) + pow(y, 2);
	dist = sqrt(dist);

	l_result = dist <= radius + circB.radius;

	return l_result;
}

void rect() {
	cout << "Get Rect son." << endl;

	float anchorX;
	cout << "Enter rectangle 1 anchor x: ";
	cin >> anchorX;

	float anchorY;
	cout << "Enter rectangle 1 anchor y: ";
	cin >> anchorY;

	Rect rectA = Rect(21, 10, 15, 15, anchorX, anchorY);

	float anchorX2;
	cout << "Enter rectangle 2 anchor x: ";
	cin >> anchorX2;
	float anchorY2;
	cout << "Enter rectangle 2 anchor y: ";
	cin >> anchorY2;

	Rect rectB = Rect(5, 5, 15, 15, anchorX2, anchorY2);

	if (rectA.didIntersectRect(rectB))
	{
		cout << "\nA and B touchy touchied";
	}
	else
	{
		cout << "\nA and B did not touchy touchy";
	}
}

void circ() {
	cout << "Get circ son." << endl;


	Circ circA = Circ(0, 0, 10);
	Circ circB = Circ(0, 15, 5);


	if (circA.didIntersectCirc(circB))
	{
		cout << "\nA and B touchy touchied";
	}
	else
	{
		cout << "\nA and B did not touchy touchy";
	}
}



int main()
{
	int a;

	cout << "Choose: " << endl;
	cout << "   1. Rectangle " << endl;
	cout << "   2. Circle" << endl;
	cin >> a;

	if (a == 1)
		rect();
	else if (a == 2)
		circ(); 


	float dummy;

	cin >> dummy;
	return 0;
}