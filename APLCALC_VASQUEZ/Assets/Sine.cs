﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sine : MonoBehaviour {

    public float frequency;
    public float amplitude;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = (new Vector3(transform.position.x + Time.deltaTime , Mathf.Sin(transform.position.x * frequency) * amplitude, transform.position.z));
	}
}
