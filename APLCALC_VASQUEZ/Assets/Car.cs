﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour {

    float posX ;
    float posY ;
    public GameObject x;
    // Use this for initialization
    void Start () {
        posX = transform.position.x;
        posY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
        posX+=Time.deltaTime;
        posY = getValue(posX);
        Vector3 dir = new Vector3(posX, posY, 0f) - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.position = (new Vector3(posX, posY, transform.position.z));

        posY += Time.deltaTime;
        Instantiate(x, transform.position, Quaternion.identity);
    }

    float getY(float x)
    {
        return x * x * Time.deltaTime * Mathf.Sin(x * 10f) + 190f;
    }

    float getValue(float x)
    {
        return x * x * Time.deltaTime * Mathf.Sin(x * 10f);
    }
}
